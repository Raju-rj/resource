<?php



$factory->define(\App\Vehicle::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name(120),//$maxNbChars = 120
        'bookable' => 'Yes',
        'picture' => $faker->image(public_path('upload/pictures'), 900, 300, 'people', false),
        'registration_number'=>$faker->numberBetween($min = 20000, $max = 90000),
        'notes'=>$faker->sentence(5),
        'created_at' => $faker->dateTimeThisYear(),
    ];
});
