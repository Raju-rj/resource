@extends('master_layout.layout')
@section('content')
    <div class="container"><br>

        <div class="card">
            <div class="card-header">
                    <h3>Vehicle List</h3>
            </div>
            <div class="card-body">
                <div class="col-sm-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Bookable</th>
                            <th scope="col">Picture</th>
                            <th scope="col">Registration</th>
                            <th scope="col">Note</th>
                           <th scope="col" colspan="3">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($vehicles as $vehicle)
                            <tr>
                                <td>{{$vehicle->name}}</td>
                                <td>{{$vehicle->bookable}}</td>
                                <td><img src="{{asset('upload/pictures/'.$vehicle->picture)}}" width="80px" height="50px"></td>
                                <td>{{$vehicle->registration_number}}</td>
                                <td>{{$vehicle->notes}}</td>
                                <td><a class="btn btn-outline-primary" href="{{ url('vehicle/'.$vehicle->id) }}">View</a></td>
                                <td><a class="btn btn-outline-success" href="{{ url('vehicle/'.$vehicle->id.'/edit') }}"> Edit </a></td>
                                <td><form action="{{ url('vehicle/'.$vehicle->id) }}" method="post" style="display: inline">
                                        @csrf
                                        {{ method_field('delete') }}
                                        <button type="submit" class="btn btn-outline-danger" onclick="return confirm('Are You Sure Want To Delete ?')">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        <h5 class="text-right"><a href="vehicle/create" class="btn btn-success"><i class="fa-1x fa fa-plus-circle"></i> Add</a></h5>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection